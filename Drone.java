package droneGUI;

import dronepProject.ConsoleCanvas;
import dronepProject.Direction;
import dronepProject.Drone;
import dronepProject.DroneArena;
import javafx.scene.image.Image;
import solarSystemProject.MyCanvas;
import solarSystemProject.SolarSystem;

public class Drone {
	private double xPos, yPos;//Position variables
	Direction movement; //Enum dictating direction it will move next
	Image test = new Image(getClass().getResourceAsStream("drone.png"));

	 static char incrementor = '0';//ways to ID drones
	 char id;
	

	public Drone(double x,double y, Direction dir){
		
		xPos = x;//set drones co-ordinates and direction
		yPos = y;
		id = incrementor++;
		movement = dir;
	}
	public boolean isHere (int sx, int sy) {//determines whether there is a drone in a certain position
		if (xPos == sx && yPos == sy){
				return true;
		}
			else{
				return false;
			}
		}
	public String toString(){
		String pos = "Drone ";
		pos += id;
		pos += " is at position " + getX() + ", " + getY() + ", " + movement  + ".\n";
		
		return pos;		
		
		
	}
//	public void updatePosition(double tAngle){
//		tAngle = Math.toRadians(tAngle);
//		angle = tAngle * speed ;
//		 
//	}
	public void displayDrone(droneGUI.MyCanvas c) {
		// call the showIt method in c to put a D where the drone is
		
		c.drawImage(test,xPos, yPos, 40);
		}
//	public void  tryToMove(DroneArena c) {
//		movement = movement.nextDirection();
//		if(movement == Direction.North) {
//			//change value of position dependent on direction
//			xPos--;
//			if(c.canMoveHere(xPos, yPos) == false) {
//				xPos++;
//				
//			}
//			
//		}
//		if(movement == Direction.South) {
//			//change value of position dependent on direction
//			xPos++;
//			if(c.canMoveHere(xPos, yPos) == false) {
//				xPos--;
//			
//			}
//		}
//		if(movement == Direction.East) {
//			//change value of position dependent on direction
//			yPos++;
//			if(c.canMoveHere(xPos, yPos) == false) {
//				yPos--;
//			
//			}
//			
//		}
//		if(movement == Direction.West) {
//			//change value of position dependent on direction
//			yPos--;
//			if(c.canMoveHere(xPos, yPos) == false) {
//				yPos++;
//				
//			}
//			
//		}
//		
//	}
	double getX() {
		return xPos;
	}
	double getY() {
		return yPos;
	}
	public static void main(String[] args) {
		Drone d = new Drone(5, 3, Direction.North); // create drone
		Drone e = new Drone(7,2, Direction.South);
		System.out.println(d.toString());// print where is
		System.out.println(e.toString());
		
	}


}