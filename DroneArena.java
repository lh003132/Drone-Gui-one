package droneGUI;

import java.util.ArrayList;
import java.util.Random;

import dronepProject.Direction;
import javafx.scene.image.Image;




public class DroneArena {
	double yAxis, xAxis;
	ArrayList<Drone> manyDrones = new ArrayList<Drone>();
	Drone adder;
	DroneArena(int width, int height){
	
			xAxis = width;
			yAxis = height;
	
	}
	public void drawSystem(droneGUI.MyCanvas mc) {
		for(Drone d: manyDrones){
			d.displayDrone( mc);
		}
	}
//		public void drawImage (MyCanvas mc, Image i, double x, double y, double sz) {
//			int cs = mc.getXCanvasSize();
//			mc.drawImage (i, (x+sunX)*cs, (y+sunY)*cs, sz*cs);		// add 0.5 to positions then * canvas size
//		
//		}
	Drone addDrone() {
		Random randomGenerator;
		randomGenerator = new Random();
		double xCoordinate = (Math.random() * ((xAxis - 0) + 1)) + 0;//generate random x and y position
		double yCoordinate = (Math.random() * ((yAxis - 0) + 1)) + 0;
		
		adder = new Drone(xCoordinate, yCoordinate, Direction.returnDir());// create new drone
	
		manyDrones.add(adder);//add new drone to array list
		
		return adder;
	}
	
	String tostring() {
		String arena = "The arena is " + xAxis + " wide, " + yAxis + " high.\n";
		for(int i = 0; i < manyDrones.size(); i++) {   
		   arena += manyDrones.get(i).toString() ;
		}
		return arena;
	}
	
	
	}

